﻿namespace VideoStore
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileBackup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileRestore = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFillin = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFillinCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFillinMovie = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRentals = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRentalsRentmovie = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRentalsReturnmovie = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReports = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReportsRentalsdue = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReportsSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReportsSearchMovie = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReportSearchCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpContents = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuFillin,
            this.mnuRentals,
            this.mnuReports,
            this.mnuHelp});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(378, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileBackup,
            this.mnuFileRestore,
            this.mnuFileSeparator1,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "&File";
            // 
            // mnuFileBackup
            // 
            this.mnuFileBackup.Name = "mnuFileBackup";
            this.mnuFileBackup.Size = new System.Drawing.Size(122, 22);
            this.mnuFileBackup.Text = "&Backup...";
            // 
            // mnuFileRestore
            // 
            this.mnuFileRestore.Enabled = false;
            this.mnuFileRestore.Name = "mnuFileRestore";
            this.mnuFileRestore.Size = new System.Drawing.Size(122, 22);
            this.mnuFileRestore.Text = "&Restore...";
            // 
            // mnuFileSeparator1
            // 
            this.mnuFileSeparator1.Name = "mnuFileSeparator1";
            this.mnuFileSeparator1.Size = new System.Drawing.Size(119, 6);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Size = new System.Drawing.Size(122, 22);
            this.mnuFileExit.Text = "&Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // mnuFillin
            // 
            this.mnuFillin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFillinCustomer,
            this.mnuFillinMovie});
            this.mnuFillin.Name = "mnuFillin";
            this.mnuFillin.Size = new System.Drawing.Size(47, 20);
            this.mnuFillin.Text = "Fill &In";
            // 
            // mnuFillinCustomer
            // 
            this.mnuFillinCustomer.Name = "mnuFillinCustomer";
            this.mnuFillinCustomer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mnuFillinCustomer.Size = new System.Drawing.Size(166, 22);
            this.mnuFillinCustomer.Text = "&Customer";
            // 
            // mnuFillinMovie
            // 
            this.mnuFillinMovie.Name = "mnuFillinMovie";
            this.mnuFillinMovie.Size = new System.Drawing.Size(166, 22);
            this.mnuFillinMovie.Text = "&Movie";
            // 
            // mnuRentals
            // 
            this.mnuRentals.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuRentalsRentmovie,
            this.mnuRentalsReturnmovie});
            this.mnuRentals.Name = "mnuRentals";
            this.mnuRentals.Size = new System.Drawing.Size(57, 20);
            this.mnuRentals.Text = "&Rentals";
            // 
            // mnuRentalsRentmovie
            // 
            this.mnuRentalsRentmovie.Name = "mnuRentalsRentmovie";
            this.mnuRentalsRentmovie.Size = new System.Drawing.Size(154, 22);
            this.mnuRentalsRentmovie.Text = "&Rent a movie";
            // 
            // mnuRentalsReturnmovie
            // 
            this.mnuRentalsReturnmovie.Name = "mnuRentalsReturnmovie";
            this.mnuRentalsReturnmovie.Size = new System.Drawing.Size(154, 22);
            this.mnuRentalsReturnmovie.Text = "Re&turn a movie";
            // 
            // mnuReports
            // 
            this.mnuReports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuReportsRentalsdue,
            this.mnuReportsSearch});
            this.mnuReports.Name = "mnuReports";
            this.mnuReports.Size = new System.Drawing.Size(59, 20);
            this.mnuReports.Text = "Re&ports";
            // 
            // mnuReportsRentalsdue
            // 
            this.mnuReportsRentalsdue.Name = "mnuReportsRentalsdue";
            this.mnuReportsRentalsdue.Size = new System.Drawing.Size(136, 22);
            this.mnuReportsRentalsdue.Text = "Rentals &Due";
            // 
            // mnuReportsSearch
            // 
            this.mnuReportsSearch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuReportsSearchMovie,
            this.mnuReportSearchCustomer});
            this.mnuReportsSearch.Name = "mnuReportsSearch";
            this.mnuReportsSearch.Size = new System.Drawing.Size(136, 22);
            this.mnuReportsSearch.Text = "&Search";
            // 
            // mnuReportsSearchMovie
            // 
            this.mnuReportsSearchMovie.Name = "mnuReportsSearchMovie";
            this.mnuReportsSearchMovie.Size = new System.Drawing.Size(189, 22);
            this.mnuReportsSearchMovie.Text = "Search for a &movie...";
            // 
            // mnuReportSearchCustomer
            // 
            this.mnuReportSearchCustomer.Name = "mnuReportSearchCustomer";
            this.mnuReportSearchCustomer.Size = new System.Drawing.Size(189, 22);
            this.mnuReportSearchCustomer.Text = "Search for a &customer";
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpContents,
            this.mnuHelpAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(44, 20);
            this.mnuHelp.Text = "&Help";
            // 
            // mnuHelpContents
            // 
            this.mnuHelpContents.Name = "mnuHelpContents";
            this.mnuHelpContents.Size = new System.Drawing.Size(179, 22);
            this.mnuHelpContents.Text = "&Contents";
            // 
            // mnuHelpAbout
            // 
            this.mnuHelpAbout.Name = "mnuHelpAbout";
            this.mnuHelpAbout.Size = new System.Drawing.Size(179, 22);
            this.mnuHelpAbout.Text = "&About Video Store...";
            this.mnuHelpAbout.Click += new System.EventHandler(this.mnuHelpAbout_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 300);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "frmMain";
            this.Text = "Video Store";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileBackup;
        private System.Windows.Forms.ToolStripMenuItem mnuFileRestore;
        private System.Windows.Forms.ToolStripSeparator mnuFileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mnuFillin;
        private System.Windows.Forms.ToolStripMenuItem mnuFillinCustomer;
        private System.Windows.Forms.ToolStripMenuItem mnuFillinMovie;
        private System.Windows.Forms.ToolStripMenuItem mnuRentals;
        private System.Windows.Forms.ToolStripMenuItem mnuRentalsRentmovie;
        private System.Windows.Forms.ToolStripMenuItem mnuRentalsReturnmovie;
        private System.Windows.Forms.ToolStripMenuItem mnuReports;
        private System.Windows.Forms.ToolStripMenuItem mnuReportsRentalsdue;
        private System.Windows.Forms.ToolStripMenuItem mnuReportsSearch;
        private System.Windows.Forms.ToolStripMenuItem mnuReportsSearchMovie;
        private System.Windows.Forms.ToolStripMenuItem mnuReportSearchCustomer;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpContents;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpAbout;
    }
}

